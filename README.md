Fsharp.Actor
============

This is a fork off of the code from http://colinbull.github.io/Fsharp.Actor

I am just playing with F# and seeing if I can get it to work with nanomsg.

An actor framework for F#

See the samples folder for usage examples.
